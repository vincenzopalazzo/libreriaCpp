# libreriaCpp
Libreria scritta in C++ contenente alcuni comandi utili
- ==========|Funzioni incluse |============
- Leggere una stringa con controllo su stringa vuota.
- Controlla il nome del file che si vuole caricare, riesce ad aggiungere l'estensione al file se l'utente non lo digita.
  -Bug: non riconosce altre estensioni al di fuori di .txt quindi se ad esempio inseriamo nomeFile.cpp, lui non riconosce l'estenzione,
  quindi aggiunge a nomeFile.cpp l'estensione .txt (credo di risolverlo a breve).
-Procedura "strsp" elimina un eventuale spazio all'inzio di una stringa "Funzione contenuta nella libreria string.h in c"
  -Bug: Legge solo uno spazio, quindi se la stringa e' preceduta da due o più spazi lei non è in grado di correggere tutta la stringa.(cercherò di correggere a breve)
- Funzioni tic e toc: Ricreate le funzioni per il calcolo del tempo di un esecuzione del programma come in linguaggio Matlab.
 	-Bug: Non credo che si possa chiamare bug, ma se l'applicazione sta in esezuzione senza fare nulla le funzioni tic e toc riporteranno il 			tempo totale della durata dell'esecuzione del programma.
- Funzione che ha il compito di restituire il numero delle cifre da cui è composto il numero inserito.
  - Bug: Ancora non ho riscontato bug da segnalare!
------- Altre Funzioni --------
        !coming soon!
