#include "libreriaPalazzo.h"
using namespace std;
// ---------------------------------- Libreia Vincenzo Palazzo ------------------------------------------------------


void controllaEstenzione(string &nomefile){
	int lunghezza;
	string estenzione;
	lunghezza = nomefile.size();
	estenzione = nomefile.substr(lunghezza - 4, lunghezza);
	//cout << "****" << estenzione;
	if(estenzione != ".txt"){
		estenzione = ".txt";
		nomefile += estenzione;
	}
	//cout << "*****" << nomefile; // Stampe di loggin
	return;
}


void leggiStringa(string &stringaGenerica){
	getline(cin, stringaGenerica);
	while(stringaGenerica.size() == 0){
		cout << "=========| ERRORE |===========" << endl;
		cout << "Valore inserito non corretto (non può essere vuoto). Rinserisci: ";
		getline(cin, stringaGenerica);
	}
	return;
}


void strsp(string &stringaGenerica){
   if(stringaGenerica.substr(0, 1) == " "){
       stringaGenerica.erase(0, 1);
   }
   return;
}

int tic(){
	return clock();
}

float toc(int tempoIniziale){
	return (clock() - tempoIniziale) * 0.01;
}

int contaCifre(float numero){
	int cifre = 1;
	float calcola = numero;
	while(true){
		calcola /=  10;
		//cout <<  "******" << calcola << endl; // Stampe di loggin
		if(int(calcola) == 0){
			//cout << " out \n"; // Stampe di loggin
			break;
		}
		cifre++;
		//cout << cifre << endl; // Stampe di loggin
	}
	return cifre;
}


//  --------- CONTINUANDO A GIOCARE CON GITHUB --------------
//Update 05/04/2017
//@Author vincenzopalazzo