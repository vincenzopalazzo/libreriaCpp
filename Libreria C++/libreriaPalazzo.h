#include <iostream>
#include <cstring>
#include <ctime>
using namespace std;

// --------------------------------- LIBRERIA VINCENZO PALAZZO ------------------------------------------------
//               ------------------- FILE CONTENENTE I PROTOTIPI ------------------------------


void controllaEstenzione(string &nomefile);
// ---------------------------------- INFORMAZIONI PER L'USO ----------------------------------------------------------
/* Questo sottoprogramma prende in ingresso il nome di un file.
Si puo' utilizzare quando si ha la necessità di caricare un file, e permette di aggirare l'errore
dell utente che non specifica l'estenzione del file in questo caso è capace di correggere solo l'estenzione ".txt".
Il sottoprogramma e la libreria verranno aggiornati, per cercare di risolvere bug*/


void leggiStringa(string &stringaGenerica);
// ---------------------------------- INFORMAZIONI PER L'USO ----------------------------------------------------------
/* credo che il programma si commenta da solo.
Questo sottoprogramma legge una stringa generica con il suo rispettivo controllo molto generico, verifica se la lunghezza della
stringa non deve pari a 0.*/

void strsp(string &stringaGenerica);
/*Esiste una funzione predefinita in C per eliminare lo spazio iniziale dopo una lettura da file di una stringa.
Credo che esista anche in C++, ho voluto provare a fare un algortmo mio personale.
prende in entrata una stringa che controlla che l'elemento in prima posizione(0) non sia uguale allo spazio vuoto, se la condizione si verfica
elimina la sottostringa nella prima posizione.*/

int tic();
/*Ricreato la procedura con l'idea di alcune funzioni di matlab.
Questo sottoprogramma restituisce il valore del clock di sistema.*/

float toc(int tempoIniziale);
/*Ricreato la procedura con l'idea di alcune funzioni di matlab.
Questo sottoprogramma restituisce il valore del clock di sistema.*/


 //--------------- ESEMPIO FUNZIONAMENTO FUNZIONI TIC E TOC-------------
/* int main(){
	int tempoIniziale, tempoFinale;
	float tempoTot;
	tempoIniziale = tic();
	cout << "Tempo Iniziale: " << tempoIniziale << endl;
	for(int i = 0; i < 100; i++){
		if(true){
			*SERVE SOLO A FAR TRASCORRERE UN PO DI TEMPO IN PIU*
			continue;
		}
	}
	tempoTot = toc(tempoIniziale);
	cout << "Tempo tolate esecuzione: " << tempoTot << " secondi" << endl;
	return 0;
}*/

int contaCifre(float numero);
/* Questa funzione ha il compito di restituire il numero delle cifre da cui e' composto il numero inserito.
il funzionamento di questa funzione e' banale sfrutta semplicemente una divisione per 10!*/


//  --------- CONTINUANDO A GIOCARE CON GITHUB --------------
